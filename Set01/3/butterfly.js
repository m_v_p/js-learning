(function (){
    window.onload = link;
    function link() {
        const prefix = "butterflyLeft";
        var lefts = Array.from(document.querySelectorAll(`[id^=${prefix}]`));
        var ids = lefts.map(v => v.id.substr(prefix.length));

        for(let i = 0; i < ids.length; i++){
            subscribe(ids[i]);
        }
    }

    function GetElement(prefix, id){
        return document.getElementById(prefix + id);
    }

    function moveAll(from, to){
        var options = from.options;
        var count = options.length;
        for(let i = 0; i < count; i++) {
            let copy = from[0].cloneNode(true);

            from.remove(0);
            to.appendChild(copy);
        }
    }

    function moveSelected(from, to){
        var selIndex = from.selectedIndex;
        if (selIndex == -1) {
            window.alert("Выберите элемент!")
        } else {
            while(selIndex != -1)
            {
                let copy = from[selIndex].cloneNode(true);

                from.remove(selIndex);
                to.appendChild(copy);
                selIndex = from.selectedIndex;
            }
        }
    }

    function subscribe(id){
        var left = GetElement("butterflyLeft", id);
        var right = GetElement("butterflyRight", id);

        GetElement("butterflyToRightAll", id).onclick = () => moveAll(left, right);
        GetElement("butterflyToRightOne", id).onclick = () => moveSelected(left, right);
        GetElement("butterflyToLeftOne", id).onclick = () => moveSelected(right, left);
        GetElement("butterflyToLeftAll", id).onclick = () => moveAll(right, left);
    }

})();