(function () {
    window.onload = link;

    function link() {
        document.getElementById("stopButton").onclick = stopTimer;
        document.getElementById("playButton").onclick = startTimer;
        document.getElementById("prevButton").onclick = movePrev;
        document.getElementById("nextButton").onclick = moveNext;
        document.getElementById("restartButton").onclick = restart;
        document.getElementById("closeButton").onclick = () => window.close();

        if(isLast){
            showFinish(true);
        }else{
            startTimer();
        }
    }


    const pages = [
        "index.html",
        "indexTwo.html",
        "indexThree.html",
        "indexFour.html",
        "indexLast.html"
    ];

    let current = document.location.pathname.split("/").slice(-1)[0];
    let index = pages.indexOf(current);
    let isLast = index == pages.length - 1;
    let timer;



    function setText(s){
        document.getElementById("timer").innerText = s;
    }


    function startTimer(){
        function labelTimer() {
            setText(time / 1000 + "s");
            if (time == 0){
                stopTimer();
                moveNext();
            }

            time -= 1000;
        }

        var time = 3000;
        labelTimer();
        timer = setInterval(labelTimer, 1000);
    }


    function stopTimer(){
        if (timer != undefined) {
            clearInterval(timer);
            setText("");
        }
    }


    function moveNext(){
        if(!isLast){
            document.location.replace(pages[++index]);
        }
    }


    function movePrev(){
        if (index > 0)
            document.location.replace(pages[--index]);
    }


    function showFinish(finish){
        let cName = finish ? "" : "hidden";
        document.getElementById("restartButton").className = cName;
        document.getElementById("closeButton").className = cName;
    }


    function restart(){
        document.location.replace(pages[0]);
    }


})();