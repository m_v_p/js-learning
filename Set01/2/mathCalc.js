(function () {
    window.onload = function () {
        document.getElementById("getResultButton").onclick = proceed;
    }

    function proceed() {
        function ExtractNumber(array){
            let value = array.shift();
            if (value == "-"){
                return -Number(array.shift());
            } else{
                return Number(value);
            }
        }

        function calculate(formula) {
            var ops = {
                "+": function (res, ar) {
                    return res + ExtractNumber(ar);
                },
                "-": function (res, ar) {
                    return res - ExtractNumber(ar);
                },
                "*": function (res, ar) {
                    return res * ExtractNumber(ar);
                },
                "/": function (res, ar) {
                    return res / ExtractNumber(ar);
                }
            };

            let parts = formula.split(/(?<=[-+*/])|(?=[-+*/])/);

            var res = ExtractNumber(parts);
            while (parts.length > 0) {
                let current = parts.shift();

                if (current in ops){
                    res = ops[current](res, parts);
                }
                else{
                    return undefined;
                }
            }
            return res;
        }


        var inputTxt = document.getElementById("inputText");
        var outputTxt = document.getElementById("resultText");

        var s = inputTxt.value;

        s = s.replace(/\s+/g, '');

        var res;
        if (!s.endsWith("="))
            res = "Where is =?";
        else
            res = calculate(s.substr(0, s.length - 1));

        outputTxt.value = res.toFixed(2);
    }
})();